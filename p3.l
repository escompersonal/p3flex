DIGITO [0-9]
LETRA [a-zA-Z]
SIGNOS (-|"+")
VARIABLE ({LETRA}|_)(({DIGITO}|{LETRA}|_)*)
INT_UNSIGNED ({DIGITO}+)
INT_SIGNED {SIGNOS}({DIGITO}+)
FLOAT_UNSIGNED {INT_UNSIGNED}"."{DIGITO}*
FLOAT_SIGNED {INT_SIGNED}"."{DIGITO}*
OPERANDO ({INT_SIGNED}|{INT_UNSIGNED}|{FLOAT_SIGNED}|{FLOAT_UNSIGNED}|{VARIABLE})
POW {OPERANDO}"^"({OPERANDO})
OPERADOR (=|"+"|-|"*"|"/")
MOD (MOD|mod)"("[ ]*({OPERANDO}),{OPERANDO}[ ]*")"
MATH {OPERANDO}{OPERADOR}{OPERANDO}|{MOD}






%%
^{VARIABLE}$ {printf("Nombre de variable");}
^{INT_SIGNED}$ {printf("Numero entero con signo");}
^{INT_UNSIGNED}$ {printf("Numero entero sin signo");}
^{FLOAT_UNSIGNED}$ {printf("Decimal sin signo");}
^{FLOAT_SIGNED}$ {printf("Decimal con signo");}
^{POW}$ {printf("Potencia");}
^{MATH}$ {printf("Operacion matematica");}
. {}
%%